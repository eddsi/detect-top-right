import os
from src.utils.config_manager import ConfigManager
from src.models.model_train import ModelTrainer


def test_ModelTrainer():
    data_dir = r"D:\01WorkingDirectory\train"
    config_manager = ConfigManager("../src/config/AH200.json")
    model_trainer = ModelTrainer(data_dir, config_manager)
    model_trainer.run()


if __name__ == '__main__':
    test_ModelTrainer()
