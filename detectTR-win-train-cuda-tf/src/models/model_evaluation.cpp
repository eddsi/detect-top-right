#include <iostream>
#include <chrono>
#include "base_model.h"
#include "data_preprocessor.h"

int main() {
    CoDeNet net;
    torch::load(net, "../conet.pt");
    torch::Device device = torch::kCPU;
    net->to(device);
    net->eval();
    torch::NoGradGuard no_grad;
    std::string path_train = R"(D:\01WorkingDirectory\train\)";
    std::string path_val = R"(D:\01WorkingDirectory\val\)";
    std::string annotations_train = R"(../train.txt)";
    std::string annotations_val = R"(../val.txt)";
    auto custom_dataset_val = DataLoader(path_val, annotations_val, 1, 224, 224).map(
            torch::data::transforms::Stack<>());;
    auto data_loader_val = torch::data::make_data_loader<torch::data::samplers::RandomSampler>(
            std::move(custom_dataset_val), torch::data::DataLoaderOptions(32));

    int total_correct = 0;
    int total_samples = 0;
    int true_positive = 0;
    int false_positive = 0;
    int false_negative = 0;

    for (auto &batch: *data_loader_val) {
        auto data = batch.data.to(torch::kF32).to(device);
        auto targets = batch.target.to(torch::kF32).to(device);
        auto outputs = net->forward(data);
        auto predictions = outputs.round();

        total_correct += predictions.eq(targets).sum().item<int>();
        total_samples += targets.size(0);

        for (int i = 0; i < targets.size(0); ++i) {
            if (predictions[i].item<int>() == 1) {
                if (targets[i].item<int>() == 1) {
                    true_positive++;
                } else {
                    false_positive++;
                }
            } else {
                if (targets[i].item<int>() == 1) {
                    false_negative++;
                }
            }
        }
    }

    float accuracy = static_cast<float>(total_correct) / total_samples;
    float recall = static_cast<float>(true_positive) / (true_positive + false_negative);
    float precision = static_cast<float>(true_positive) / (true_positive + false_positive);
    float f1 = 2 * (precision * recall) / (precision + recall);

    std::cout << "Accuracy: " << accuracy << std::endl;
    std::cout << "Recall: " << recall << std::endl;
    std::cout << "Precision: " << precision << std::endl;
    std::cout << "F1 Score: " << f1 << std::endl;

    return 0;
}
