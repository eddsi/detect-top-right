#include <cuda_runtime_api.h>
#include "detectTR.h"
#include <iostream>
#include <NvInfer.h>
#include <cuda_runtime_api.h>
#include <iostream>
#include <fstream>
#include <cassert>
#include <opencv2/opencv.hpp>
#include "inline.h"

using namespace nvinfer1;
nvinfer1::IRuntime *gRuntime = nullptr;
nvinfer1::ICudaEngine *gEngine = nullptr;
nvinfer1::IExecutionContext *gContext = nullptr;
void *gBuffers[2] = {nullptr, nullptr};
const int INPUT_C = 1;
const int INPUT_H = 224;
const int INPUT_W = 224;
const int max_batch_size = 1;
size_t inputSize = INPUT_H * INPUT_W * INPUT_C * sizeof(float);

class Logger : public ILogger {
    void log(Severity severity, const char *msg) noexcept override {}
} gLogger;

bool constructNet(){
    gRuntime = nvinfer1::createInferRuntime(gLogger);
    if (!gRuntime) {
        std::cerr << "Failed to create Infer Runtime" << std::endl;
        return false;
    }
    std::string enginePath = "../TPClassifier.trt";
    std::ifstream file(enginePath, std::ios::binary);
    if (!file.good()) {
        std::cerr << "Failed to open engine file: " << enginePath << std::endl;
        return false;
    }

    file.seekg(0, file.end);
    size_t size = file.tellg();
    file.seekg(0, file.beg);
    std::vector<char> buffer(size);
    file.read(buffer.data(), size);
    file.close();

    gEngine = gRuntime->deserializeCudaEngine(buffer.data(), size, nullptr);
    if (!gEngine) {
        std::cerr << "Failed to create CUDA Engine" << std::endl;
        return false;
    }

    gContext = gEngine->createExecutionContext();
    if (!gContext) {
        std::cerr << "Failed to create Execution Context" << std::endl;
        return false;
    }

    size_t inputSize = INPUT_H * INPUT_W * INPUT_C * sizeof(float);
    cudaMalloc(&gBuffers[0], max_batch_size * inputSize);
    cudaMalloc(&gBuffers[1], max_batch_size * sizeof(float));
    std::cout << "Construct net successfully" << std::endl;
    return true;
}

bool inference(ImgData img){
    Mat outputMat;
    int message = img2Mat(img, outputMat);
    outputMat.convertTo(outputMat, CV_32FC1, 1.0 / 255);
    cv::Mat resized;
    cv::resize(outputMat, resized, cv::Size(224, 224));
    cudaMemcpy(gBuffers[0], resized.ptr<float>(0), inputSize, cudaMemcpyHostToDevice);
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    gContext->setOptimizationProfileAsync(0, stream);
    gContext->setBindingDimensions(0, Dims4(max_batch_size, INPUT_H, INPUT_W, INPUT_C));
    gContext->executeV2(gBuffers);
    std::vector<float> output(max_batch_size);
    cudaMemcpy(output.data(), gBuffers[1], max_batch_size * sizeof(float), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    cudaStreamDestroy(stream);
    return output[0] > 0.5;
}

void freeSource(){
    if (gContext) {
        gContext->destroy();
        gContext = nullptr;
    }
    if (gEngine) {
        gEngine->destroy();
        gEngine = nullptr;
    }
    if (gRuntime) {
        gRuntime->destroy();
        gRuntime = nullptr;
    }
    cudaFree(gBuffers[0]);
    cudaFree(gBuffers[1]);
}
