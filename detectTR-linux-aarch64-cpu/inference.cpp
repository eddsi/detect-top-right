#include <torch/torch.h>
#include <iostream>
#include <chrono>
#include "base_model.h"
#include "data_preprocessor.h"


int main() {
    CoDeNet net;
    torch::load(net, "../conet.pt");
    torch::Device device = torch::kCPU;
    net->to(device);
    net->eval();
    torch::NoGradGuard no_grad;

    // 定义输入数据的尺寸
    const int num_tests = 100; // 测试次数
    const int batch_size = 1;  // 每批处理的样本数
    const int channels = 1;    // 输入通道数
    const int height = 224;    // 图像高度
    const int width = 224;     // 图像宽度

    std::vector<double> times; // 存储每次推理的运行时间

    for (int i = 0; i < num_tests; ++i) {
        // 生成随机输入数据
        auto input = torch::randn({batch_size, channels, height, width}).to(torch::kF32).to(device);

        // 开始计时
        auto start = std::chrono::high_resolution_clock::now();

        // 执行推理
        auto output = net->forward(input);

        // 结束计时
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> duration = end - start;

        // 保存本次推理时间
        times.push_back(duration.count());
    }

    // 计算平均运行时间
    double total_time = 0;
    for (auto& time : times) {
        total_time += time;
    }
    double avg_time = total_time / num_tests;

    std::cout << "Average Inference Time: " << avg_time << " ms" << std::endl;

    return 0;
}