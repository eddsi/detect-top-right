#include "base_model.h"

CoDeNetImpl::CoDeNetImpl() : conv1(torch::nn::Conv2dOptions(1, 16, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv2(torch::nn::Conv2dOptions(16, 16, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv3(torch::nn::Conv2dOptions(16, 32, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv4(torch::nn::Conv2dOptions(32, 32, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv5(torch::nn::Conv2dOptions(32, 64, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv6(torch::nn::Conv2dOptions(64, 64, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv7(torch::nn::Conv2dOptions(64, 128, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv8(torch::nn::Conv2dOptions(128, 128, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             conv9(torch::nn::Conv2dOptions(128, 512, 3)
                                           .stride(1)
                                           .padding(1)
                                           .bias(true)),
                             maxpool(torch::nn::MaxPool2dOptions(2).stride(2).padding(0)),
                             fc1(torch::nn::Linear(25088, 64)
                             ),
                             fc2(torch::nn::Linear(64, 64)
                             ),
                             fc3(torch::nn::Linear(64, 1)
                             ) {
    register_module("conv1", conv1);
    register_module("conv2", conv2);
    register_module("conv3", conv3);
    register_module("conv4", conv4);
    register_module("conv5", conv5);
    register_module("conv6", conv6);
    register_module("conv7", conv7);
    register_module("conv8", conv8);
    register_module("conv9", conv9);
    register_module("maxpool", maxpool);
    register_module("fc1", fc1);
    register_module("fc2", fc2);
    register_module("fc3", fc3);
}

torch::Tensor CoDeNetImpl::forward(torch::Tensor x) {

    x = torch::relu(conv1(x));
    //std::cout << "input:" << x << std::endl;
    x = maxpool(x);

    x = torch::relu(conv2(x));
    x = torch::relu(conv3(x));
    x = maxpool(x);

    x = torch::relu(conv4(x));
    x = torch::relu(conv5(x));
    x = maxpool(x);

    x = torch::relu(conv6(x));
    x = torch::relu(conv7(x));
    x = maxpool(x);

    x = torch::relu(conv8(x));
    x = torch::relu(conv9(x));
    x = maxpool(x);

    x = torch::flatten(x, 1);
    x = torch::relu(fc1->forward(x));
    x = torch::relu(fc2->forward(x));
    x = fc3->forward(x);
    x = torch::sigmoid(x);
    return x;
}