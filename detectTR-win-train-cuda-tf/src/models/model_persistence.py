from tensorflow.keras.models import load_model
import keras2onnx
import onnx
import tensorflow as tf


class ModelPersistence:
    def __init__(self, model_path=None):
        self.model = None
        if model_path is not None:
            self.load_model(model_path)

    def save_model(self, file_path, format='h5'):
        """
        将模型保存到指定的文件路径。

        :param file_path: 模型保存的文件路径。
        :param format: 模型的保存格式（'h5', 'tf', 'onnx'）。
        """
        if self.model is None:
            print("No model is set to be saved.")
            return

        if format == 'h5':
            self.model.save(file_path)
        elif format == 'tf':
            self.model.save(file_path, save_format='tf')
        elif format == 'onnx':
            self.save_model_as_onnx(file_path)
        else:
            print(f"Format '{format}' not supported.")

    def load_model(self, file_path):
        """
        从指定的文件路径加载模型。

        :param file_path: 模型文件的路径。
        :return: 加载的 Keras 模型实例。
        """
        self.model = load_model(file_path)

    def save_model_as_onnx(self, onnx_model_path):
        """
        将模型转换为 ONNX 格式并保存。

        :param onnx_model_path: ONNX 模型保存的路径。
        """
        if self.model is None:
            print("No model is set to be converted to ONNX.")
            return

        onnx_model = keras2onnx.convert_keras(self.model)
        onnx.save_model(onnx_model, onnx_model_path)
        print(f"Model saved as ONNX format at {onnx_model_path}.")


# 使用示例
if __name__ == '__main__':
    model_persistence = ModelPersistence("../../model_weights/conet.h5")
    model_persistence.save_model("../../model_weights/conet.h5")  # 保存为 H5 格式
    model_persistence.save_model("../../model_weights/conet_saved", format='tf')  # 保存为 TensorFlow SavedModel 格式
    model_persistence.save_model("../../model_weights/conet.onnx", format='onnx')  # 转换并保存为 ONNX 格式
