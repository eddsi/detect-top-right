from src.utils.ConfigManager import ConfigManager
from src.utils.DatasetGenerator import DatasetGenerator

# 假设 ConfigManager 已经初始化并加载了配置
config_manager = ConfigManager("../src/config/AH200.json")

# 创建 DataGenerator 实例
data_gen = DatasetGenerator(config_manager)

# 定义源和目标路径
source_path = r"C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\NonTopRight"
destination_path = r"D:\01WorkingDirectory\train\ttt/"
number_of_images = 10  # 生成图像的数量
data_gen.generate_all_data(source_path, destination_path, number_of_images)
print("Image generation completed.")

source1 = r"C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\NonTopRight"
source2 = r"C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\TopRight"

destination_train_class1 = r"D:\01WorkingDirectory\train\NonTopRight/"
destination_train_class2 = r"D:\01WorkingDirectory\train\TopRight/"
destination_test_class1 = r"D:\01WorkingDirectory\val\NonTopRight/"
destination_test_class2 = r"D:\01WorkingDirectory\val\TopRight/"

# 生成图像
data_gen.generate_all_data(source1, destination_train_class1, 8192)
data_gen.generate_all_data(source2, destination_train_class2, 8192)
data_gen.generate_all_data(source1, destination_test_class1, 512)
data_gen.generate_all_data(source2, destination_test_class2, 512)
