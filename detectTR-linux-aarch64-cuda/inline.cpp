#include "inline.h"
#define DATA_INPUT_ERROR (-1)
#define JPEG_TO_GRAY_ERROR (-2)

int img2Mat(const ImgData img, Mat& outputMat)
{
    if (img.data == NULL || img.dataLength == 0)
    {
        return DATA_INPUT_ERROR;
    }
    Mat inputMat;
    int nWidth = 0, nHeight = 0;
    vector<unsigned char> vecGrayImg;
    vector<unsigned char> vecJpeg(img.data, img.data + img.dataLength);
    int ret = jpeg2Gray(vecJpeg, 1, vecGrayImg, nWidth, nHeight);
    if (ret != 0)
    {
        return JPEG_TO_GRAY_ERROR;
    }
    inputMat = Mat(nHeight, nWidth, CV_8UC1);
    memcpy(inputMat.data, &vecGrayImg[0], nWidth * nHeight);
    outputMat = inputMat;

    return 0;
}
int jpeg2Gray(const vector<unsigned char>& vecJpegImg,
              int nImageType,
              vector<unsigned char>& vecGrayImg,
              int& nWidth,
              int& nHeight)
{
    unsigned int vecJpegImgSize = vecJpegImg.size();
    //LOGI("Alg - Jpeg2Gray begin: size={}, imgType={}.", vecJpegImgSize, nImageType);

    if (vecJpegImgSize <= 0)
    {
        return -1;
    }

    if (nImageType == 0)
    {
        Mat grayMat = imdecode(Mat(vecJpegImg), IMREAD_GRAYSCALE);
        nWidth = grayMat.cols;
        nHeight = grayMat.rows;
        vecGrayImg.clear();
        vecGrayImg.resize(nWidth * nHeight);
        memcpy(&vecGrayImg[0], grayMat.data, nWidth * nHeight);
    }
    else if (nImageType == 1)
    {
        Mat rgbMat = imdecode(Mat(vecJpegImg), IMREAD_COLOR);
        nWidth = rgbMat.cols;
        nHeight = rgbMat.rows;
        Mat grayMat;
        cvtColor(rgbMat, grayMat, COLOR_BGR2GRAY);
        vecGrayImg.clear();
        vecGrayImg.resize(nWidth * nHeight);
        memcpy(&vecGrayImg[0], grayMat.data, nWidth * nHeight);
    }
    else if (nImageType == 2)
    {
        Mat rgbMat = imdecode(Mat(vecJpegImg), IMREAD_COLOR);
        nWidth = rgbMat.cols;
        nHeight = rgbMat.rows;
        Mat grayMat;
        cvtColor(rgbMat, grayMat, COLOR_BGR2GRAY);
        vecGrayImg.clear();
        vecGrayImg.resize(sizeof(Vec3b) * nWidth * nHeight);
        memcpy(&vecGrayImg[0], rgbMat.data, sizeof(Vec3b) * nWidth * nHeight);
    }

    //LOGI("Alg - Jpeg2Gray end: nWidth={}, nHeight={}.", nWidth, nHeight);
    return 0;
}