# detectTR
### 概述
detectTR是一个专门为识别右上角设计的图像分类解决方案。此项目使用卷积
神经网络（CNN）来进行二分类任务，并支持在不同的平台和硬件配置下进行训练和推理。
### 特性
- 图像预处理和增强
- 支持在Windows和Linux平台上进行部署
- 支持GPU加速
## 快速开始
### 依赖项
本文档只讨论部署（针对Linux和Windows），训练统一在Windows平台，使用tensorflow-gpu完成。
- CMake-3.10以上
- Windows:TensorRT-8.2.1.8 Linux:TensorRT-8.2.1.9
- cuDNN-8.2.1.32 for both Windows and Linux
- OpenCV-4.7.0
- CUDA-10.2
TensorRT和cuDNN版本必须对应，请参考NVIDIA官方网站。
### 构建项目
要构建此项目，请在项目根目录下运行以下命令：
```
mkdir build
cd build
cmake ..
```
这些命令将在build目录中生成必要的构建文件。
### 编译项目
在build目录中，根据您的系统配置运行相应的编译命令。
例如，在Windows上使用MSBuild/ninja，而在Linux上使用make。
#### Windows
```
msbuild YourSolution.sln /p:Configuration=Debug
```
```
ninja
```
#### Linux
```make```
### 运行应用程序
编译完成后，可执行文件将位于build目录中，直接运行可执行文件以启动应用程序。
### 贡献
我们欢迎贡献！请阅读“CONTRIBUTION.md”了解如何参与项目。