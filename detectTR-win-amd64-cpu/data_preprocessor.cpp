#include "data_preprocessor.h"
#include <fstream>

// Constructor for DataLoader
DataLoader::DataLoader(std::string img_dir, const std::string &annotations_file, int channel, int height, int width) {
    out_channels = channel;
    out_height = height;
    out_width = width;

    // Ensure channel is either 1 or 3
    assert(channel == 1 || channel == 3);

    // Set flag based on the number of channels
    flag_open = (channel > 1) ? 1 : 0;

    std::ifstream input_file;
    try {
        // Open annotations file
        input_file.open(annotations_file, std::ios::in);
        if (!input_file) {
            std::cerr << "Error: can't open file. Please make sure input_file path is correct.\n";
            exit(-2);
        }

        // Read image paths and labels from the file
        std::string image_path;
        int64_t label;
        num_classes = 0;
        int64_t current_class = -1;
        while (input_file >> image_path >> label) {
            // Validate label values
            if (label != 0 && label != 1) {
                std::cerr << "Invalid label value: " << label << " at image path: " << image_path << std::endl;
                continue; // Skip invalid labels
            }
            // Count number of classes
            if (current_class != label) {
                num_classes++;
                current_class = label;
            }

            // Append image path and label to lists
            images_list.push_back(img_dir + image_path);
            labels_list.push_back(label);
        }
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << '\\n';
        exit(-3);
    }
    input_file.close();
}

// Function to get data and label for a given index
torch::data::Example<> DataLoader::get(size_t index) {
    // Load image
    cv::Mat img = cv::imread(images_list[index], flag_open);
    auto tdata = img_to_tensor(img);

    // Convert label to tensor
    float label_value = static_cast<float>(labels_list[index]);
    auto tlabel = torch::from_blob(&label_value, {1}, torch::kF32);

    return {tdata.clone(), tlabel.clone()};
}

// Function to return the size of the dataset
torch::optional<size_t> DataLoader::size() const {
    return images_list.size();
}

// Function to get the number of classes in the dataset
long DataLoader::get_num_classes() {
    return num_classes;
}

// Function to convert an OpenCV Mat image to a Torch tensor
torch::Tensor DataLoader::img_to_tensor(cv::Mat img) {
    assert(!img.empty());
    // Normalize the image
    img.convertTo(img, CV_32FC1);
    img = img / 255.0;
    if (!img.isContinuous()) {
        img = img.clone();
    }

    // If single channel, return the channel as is
    if (img.channels() == 1) {
        auto channel = torch::from_blob(
                img.ptr<float>(),
                {1, out_height, out_width},
                torch::kFloat);
        return channel.clone();
    }

    // Split channels and create tensor for each
    std::vector<cv::Mat> channels(3);
    cv::split(img, channels);

    auto R = torch::from_blob(
            channels[2].ptr(),
            {out_height, out_width},
            torch::kFloat);
    auto G = torch::from_blob(
            channels[1].ptr(),
            {out_height, out_width},
            torch::kFloat);
    auto B = torch::from_blob(
            channels[0].ptr(),
            {out_height, out_width},
            torch::kFloat);

    // Combine channels and return tensor
    return torch::cat({B, G, R})
            .view({3, out_height, out_width})
            .to(torch::kFloat);
}
