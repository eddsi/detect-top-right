import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import accuracy_score, recall_score, f1_score, precision_score


class ModelEvaluator:
    def __init__(self, model_path, test_data_dir):
        """
        初始化 ModelEvaluator。

        :param model_path: 要评估的模型文件路径。
        :param test_data_dir: 测试数据集的目录。
        """
        self.model = load_model(model_path)
        self.test_data_dir = test_data_dir

    def evaluate(self):
        """
        对模型进行评估，并返回评估结果。
        """
        test_datagen = ImageDataGenerator(rescale=1. / 255)
        test_generator = test_datagen.flow_from_directory(
            self.test_data_dir,
            color_mode='grayscale',
            target_size=(224, 224),
            batch_size=1,
            class_mode='binary',
            shuffle=False
        )

        predictions = self.model.predict(test_generator,
                                         steps=np.ceil(test_generator.samples / test_generator.batch_size))

        predicted_classes = np.where(predictions > 0.5, 1, 0)

        true_classes = test_generator.classes

        results = {
            "accuracy": accuracy_score(true_classes, predicted_classes),
            "precision": precision_score(true_classes, predicted_classes),
            "recall": recall_score(true_classes, predicted_classes),
            "f1_score": f1_score(true_classes, predicted_classes)
        }

        return results

    def display_results(self, results):
        """
        显示评估结果。

        :param results: 评估指标的结果字典。
        """
        for metric, value in results.items():
            print(f"{metric}: {value:.4f}")


# 使用 ModelEvaluator 的示例
if __name__ == '__main__':
    model_path = "../../model_weights/conet.h5"
    test_data_dir = r"D:\01WorkingDirectory\val"

    evaluator = ModelEvaluator(model_path, test_data_dir)
    results = evaluator.evaluate()
    evaluator.display_results(results)
