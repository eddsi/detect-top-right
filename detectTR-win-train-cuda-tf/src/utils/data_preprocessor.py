import numpy as np
from tensorflow.keras.preprocessing.image import load_img, img_to_array


class DataPreprocessor:
    def __init__(self, image_height, image_width):
        self.image_height = image_height
        self.image_width = image_width

    def preprocess_input(self, image_path):
        """加载和预处理输入图像"""
        img = load_img(image_path, target_size=(self.image_height, self.image_width), color_mode="grayscale")
        img = img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img /= 255.0
        return img


class CustomDataset:
    def __init__(self, train_data_path, val_data_path, preprocess_params=None):
        """
        初始化CustomDataset类。
        :param train_data_path: 训练数据的路径。
        :param val_data_path: 验证数据的路径。
        :param preprocess_params: 预处理参数。
        """
        self.train_data_path = train_data_path
        self.val_data_path = val_data_path
        self.preprocess_params = preprocess_params if preprocess_params else {}

    def _load_data(self, data_path):
        """
        加载数据。
        :param data_path: 数据的路径。
        :return: 加载的数据。
        """
        # 这里应该添加加载数据的代码，例如读取文件、数据库等。
        # 这里只是一个占位符，需要根据你的数据存储方式进行实现。
        data = None
        return data

    def _preprocess(self, data):
        """
        预处理数据。
        :param data: 原始数据。
        :return: 预处理后的数据。
        """
        # 添加数据预处理的步骤，这可能包括缩放、标准化、裁剪等操作。
        # 这里只是一个占位符，需要根据你的预处理需求进行实现。
        processed_data = data
        return processed_data

    def get_train_data(self):
        """
        获取预处理后的训练数据。
        :return: 训练数据。
        """
        train_data = self._load_data(self.train_data_path)
        return self._preprocess(train_data)

    def get_val_data(self):
        """
        获取预处理后的验证数据。
        :return: 验证数据。
        """
        val_data = self._load_data(self.val_data_path)
        return self._preprocess(val_data)


if __name__ == '__main__':
    source1 = r"C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\NonTopRight"
    source2 = r"C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\TopRight"

    destination_train_class1 = r"D:\01WorkingDirectory\train\NonTopRight/"
    destination_train_class2 = r"D:\01WorkingDirectory\train\TopRight/"
    destination_test_class1 = r"D:\01WorkingDirectory\val\NonTopRight/"
    destination_test_class2 = r"D:\01WorkingDirectory\val\TopRight/"

    print("success")
