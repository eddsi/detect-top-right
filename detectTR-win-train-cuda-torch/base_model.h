#ifndef DETECTTR_WIN_TRAIN_CUDA_TORCH_BASE_MODEL_H
#define DETECTTR_WIN_TRAIN_CUDA_TORCH_BASE_MODEL_H
#include <torch/torch.h>

struct CoDeNetImpl : torch::nn::Module {
public:
    CoDeNetImpl();

    torch::Tensor forward(torch::Tensor x);

private:
    torch::nn::Conv2d conv1, conv2, conv3, conv4, conv5, conv6, conv7, conv8, conv9;
    torch::nn::Linear fc1, fc2, fc3;
    torch::nn::MaxPool2d maxpool;
};

TORCH_MODULE(CoDeNet);
#endif //DETECTTR_WIN_TRAIN_CUDA_TORCH_BASE_MODEL_H
