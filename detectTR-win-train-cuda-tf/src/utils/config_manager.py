import json
import os.path


class ConfigManager:
    def __init__(self, device_model):
        self.config = self.load_config(device_model)
        self.initialize_attributes()

    def load_config(self, device_config_path):
        with open(device_config_path, 'r') as file:
            return json.load(file)

    def get_value(self, key):
        return self.config.get(key)

    def initialize_attributes(self):
        for key, value in self.config.items():
            setattr(self, key, value)


if __name__ == "__main__":
    config_manager = ConfigManager("AH200")
    print(1)
