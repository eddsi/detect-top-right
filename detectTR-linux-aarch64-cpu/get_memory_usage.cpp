#include <windows.h>
#include <psapi.h>

PROCESS_MEMORY_COUNTERS pmc;
if (GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc))) {
std::cout << "Memory Usage: " << pmc.WorkingSetSize / 1024 << " KB" << std::endl;
}