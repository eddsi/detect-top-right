#include "ModelEvaluator.h"
#include <iostream>

int main() {
    std::string model_path = "../../model_weights/TPClassifier.pt";
    std::string test_data_dir = "path/to/test/data";

    ModelEvaluator evaluator(model_path, test_data_dir);
    auto results = evaluator.evaluate();

    std::cout << "Accuracy: " << results[0] << std::endl;
    // 打印其他指标
    // ...

    return 0;
}
