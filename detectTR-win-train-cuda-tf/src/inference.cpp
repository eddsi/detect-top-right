#include <iostream>
#include <chrono>
#include "base_model.h"

int main() {
    CoDeNet net;
    torch::load(net, "../conet.pt");
    torch::Device device = torch::kCPU;
    net->to(device);
    net->eval();

    // Define the dimensions of the input data
    const int num_tests = 100; // Number of execution times for the loop
    const int batch_size = 1;  // Number of samples processed per batch
    const int channels = 1;    // Number of input channels
    const int height = 224;    // Image height
    const int width = 224;     // Image width

    std::vector<double> times; // Store the runtime for each inference
    // Generate random input data
    auto input = torch::randn({batch_size, channels, height, width}).to(torch::kF32).to(device);
    for (int i = 0; i < num_tests; ++i) {
        // Start timing
        auto start = std::chrono::high_resolution_clock::now();

        // Perform inference
        auto output = net->forward(input);

        // Stop timing
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> duration = end - start;

        // Save the inference time for this run
        times.push_back(duration.count());
    }

    // Calculate the average runtime
    double total_time = 0;
    for (auto &time: times) {
        total_time += time;
    }
    double avg_time = total_time / num_tests;

    std::cout << "Average Inference Time: " << avg_time << " ms" << std::endl;

    return 0;
}
