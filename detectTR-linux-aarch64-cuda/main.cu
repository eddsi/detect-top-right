#include "detectTR.h"
#include <iostream>
#include <fstream>
#include <cstring>

int readBufferFromFile(const char *fileName, unsigned char *buf, size_t size, unsigned long &dataLen) {
    // 打开文件
    std::ifstream file(fileName, std::ifstream::binary);
    if (!file.is_open()) {
        return -1;
    }

    // 读取文件内容
    file.read(reinterpret_cast<char*>(buf), size);
    dataLen = file.gcount();

    // 检查是否读取成功
    if (!file) {
        file.close();
        return -1;
    }

    file.close();
    return 0;
}

int main() {
    constructNet();
    const char *fileName = "/home/sino/Dataset/InfraredVisionDataset/GenerateFromHere/NonTopRight/register_00000378-1.jpg"; // 替换为您的文件路径

    // 打开文件并确定大小
    std::ifstream file(fileName, std::ifstream::binary | std::ifstream::ate);
    if (!file.is_open()) {
        return -1;
    }

    std::streamsize size = file.tellg();
    file.close();

    ImgData imgData;
    imgData.height = 2592;
    imgData.width = 1944;
    imgData.dataType = 0;

    // 分配内存并设置 dataLength
    imgData.data = new unsigned char[size];
    imgData.dataLength = static_cast<unsigned int>(size);

    // 使用 readBufferFromFile 读取数据
    unsigned long readLength = 0;
    int result = readBufferFromFile(fileName, imgData.data, imgData.dataLength, readLength);

    if (result != 0) {
        std::cout << "fail" << std::endl;
        delete[] imgData.data; // 记得释放内存
        return -1;
    }

    bool i = inference(imgData);
    delete[] imgData.data; // 记得释放内存

    std::cout << i << std::endl;

    freeSource();
    return 0;
}
