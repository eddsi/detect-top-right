#ifndef DETECTTR_WIN_AMD64_CUDA_INLINE_H
#define DETECTTR_WIN_AMD64_CUDA_INLINE_H

#include "detectTR.h"
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

int img2Mat(ImgData img, Mat &outputMat);

int jpeg2Gray(const vector<unsigned char> &vecJpegImg,
              int nImageType,
              vector<unsigned char> &vecGrayImg,
              int &nWidth,
              int &nHeight);

#endif //DETECTTR_WIN_AMD64_CUDA_INLINE_H
