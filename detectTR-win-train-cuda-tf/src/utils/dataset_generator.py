import cv2 as cv
import random
import math
import numpy as np
from numba import cuda
from .config_manager import ConfigManager
import os
import csv


@cuda.jit
def generate_single_image_4AH200(original_receipt, new_image, horizontal, vertical, theta,
                                 x_receipt_top_left, x_receipt_bottom_right,
                                 y_receipt_top_left, y_receipt_bottom_right,
                                 x_background_top_left, x_background_bottom_right,
                                 y_background_top_left, y_background_bottom_right
                                 ):
    x_in_original, y_in_original = cuda.grid(2)
    if (x_in_original >= x_receipt_top_left) and (x_in_original <= x_receipt_bottom_right) and (
            y_in_original >= y_receipt_top_left) and (y_in_original <= y_receipt_bottom_right):
        x_transformed = int(
            (x_in_original - x_receipt_top_left) * math.cos(theta) - (y_in_original - y_receipt_top_left) * math.sin(
                theta) + x_background_top_left + horizontal)
        y_transformed = int(
            (x_in_original - x_receipt_top_left) * math.sin(theta) + (y_in_original - y_receipt_top_left) * math.cos(
                theta) + y_background_top_left + vertical)
        if (x_transformed >= x_background_top_left) and (x_transformed <= x_background_bottom_right) and (
                y_transformed >= y_background_top_left) and (y_transformed <= y_background_bottom_right):
            new_image[y_transformed, x_transformed] = original_receipt[y_in_original, x_in_original]


class DatasetGenerator:
    'Generates data for different projects'

    def __init__(self, config_manager: ConfigManager):
        'Initialization'
        for key, value in config_manager.config.items():
            setattr(self, key, value)

    def __len__(self):
        'Denotes the number of batches per epoch'

    def __getitem__(self, index):
        'Generate one batch of data'
        pass

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        pass

    def generate_one_image(self, source, destination, count):
        original_receipt = cv.imread(source, cv.IMREAD_GRAYSCALE)
        background = cv.imread(self.BACKGROUND_IMAGE_PATH, cv.IMREAD_GRAYSCALE)
        new_image_device = cuda.to_device(background)
        ori_device = cuda.to_device(original_receipt)
        threadsperblock = (16, 16)
        blockspergrid_x = np.ceil(self.full_height / threadsperblock[0]).astype(int)
        blockspergrid_y = np.ceil(self.full_width / threadsperblock[1]).astype(int)

        blockspergrid = (blockspergrid_x, blockspergrid_y)
        horizontal = random.randint(0, (self.x_receipt_bottom_right - self.x_receipt_top_left) // 2)
        vertical = random.randint(0, self.full_width // 2)

        t1 = -math.atan(horizontal / (self.full_width - vertical - self.y_receipt_top_left))
        t2 = math.atan(vertical / (self.x_receipt_bottom_right - self.x_receipt_top_left - horizontal))
        angel = random.uniform(t1, t2)
        generate_single_image_4AH200[blockspergrid, threadsperblock](
            ori_device, new_image_device, horizontal, vertical, angel,
            self.x_receipt_top_left, self.x_receipt_bottom_right,
            self.y_receipt_top_left, self.y_receipt_bottom_right,
            self.x_background_top_left, self.x_background_bottom_right,
            self.y_background_top_left, self.y_background_bottom_right
        )

        cuda.synchronize()
        new_image = cv.resize(new_image_device.copy_to_host(), (self.image_height, self.image_width))
        cv.imwrite(destination + "{:05}".format(count) + ".jpg", new_image)
        count += 1
        del new_image_device
        return count

    def generate_all_data(self, source, destination, number_of_images):
        path_data = [os.path.join(source, i) for i in os.listdir(source)]
        count = 0
        for _ in range(number_of_images):
            count = self.generate_one_image(random.choice(path_data), destination, count)

    def convert_txt2csv(self, inputfilename):
        with open(inputfilename, 'r') as infile:
            with open('output.csv', 'w', newline='') as outfile:
                # Define the delimiter for the text file (e.g., comma, space, tab, etc.)
                txt_reader = csv.reader(infile, delimiter='\t')  # Here we use tab as an example
                csv_writer = csv.writer(outfile)
                for row in txt_reader:
                    # Write the row to the CSV file
                    csv_writer.writerow(row)

    def make_labels_csv(self, path):
        sub_dir_1order = os.listdir(path)
        classes = 0
        with open(f"{os.path.basename(path)}.csv", mode='w', newline='') as file:
            writer = csv.writer(file)
            for i in sub_dir_1order:
                abspath_sub_dir_1order = os.path.join(path, i)
                sub_dir_2order = os.listdir(abspath_sub_dir_1order)
                for j in sub_dir_2order:
                    writer.writerow(
                        [f"{i}/{j}", f"{classes}"]
                    )
                classes += 1

        return

    def make_labels_txt(self, path):
        sub_dir_1order = os.listdir(path)
        classes = 0
        lines_to_write = []
        for i in sub_dir_1order:
            abspath_sub_dir_1order = os.path.join(path, i)
            sub_dir_2order = os.listdir(abspath_sub_dir_1order)
            for j in sub_dir_2order:
                lines_to_write.append(f"{i}/{j} {classes}\n")
            classes += 1
        with open(f"{os.path.basename(path)}.txt", "a") as file:
            file.writelines(lines_to_write)
        return
