# 使用指南 - detectTR 静态库

## 构建静态库

运行以下命令以构建 `detectTR.lib` 静态库：

```bash
nvcc -lib -o detectTR.lib detectTR.cu inline.cpp -Xcompiler "/MD" -I D:\ProgramData\TensorRT-8.2.1.8\include -I D:\ProgramData\opencv-win-4.7.0\build\include
```
注意替换path/to/TensorRT和path/to/OpenCV为您本机上的实际路径。
或者，您也可以运行build.sh直接构建静态库。

## 必需文件
您需要以下两个文件来使用此库：

- detectTR.h: 头文件，包含库的接口定义。
- detectTR.lib: 构建的静态库文件

## 集成指南
要在您的项目中使用 detectTR 静态库，请确保将头文件和库文件包含在您的项目中。本文件夹下是一个示例的 CMakeLists.txt，演示了如何将这些文件集成到您的 CMake 项目中

在这个 CMakeLists.txt 文件中：

- 将 YourProjectName 替换为您的项目名称。
- 将 your_executable_name 替换为您的可执行文件名称。
- 将 path/to/detectTR.h 替换为 detectTR.h 头文件的实际路径。
- 将 path/to/detectTR.lib 替换为 detectTR.lib 静态库文件的实际路径。
确保您的项目目录结构正确，所有文件路径都正确指向。

## 依赖项

detectTR依赖于OpenCV、TensorRT、CUDA，请确保它们都已正确安装。

## 可用接口

detectTR对外暴露三个接口，constructNet()，inference(ImgData img)，freeSource()，以下是使用示例：

```
main(){
ImgData imgData;
/*加载图片的代码*/
bool constructSuccessfully = constructNet();
bool isVerified = inference(imgData);
freeSource();
}
```

注意，模型文件TPClassifier.trt必须与可执行文件main.exe处于同级目录！否则模型无法正确加载。