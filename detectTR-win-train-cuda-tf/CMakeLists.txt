cmake_minimum_required(VERSION 3.10)

project(CudaDetectTopRight CXX CUDA)
find_package(CUDA 10.2 EXACT REQUIRED)
set(CMAKE_CXX_STANDARD 17)

if (WIN32)
    message("Load TensorRT on Windows")
    set(TensorRT_INCLUDE_DIRS "D:/ProgramData/TensorRT-8.2.1.8/include")
    set(TensorRT_LIBS "D:/ProgramData/TensorRT-8.2.1.8/lib")
    message("Load OpenCV on Windows")
    # opencv-win是官方提供的版本，不包含OpenCV-CUDA，如果想要使用带CUDA的OpenCV，
    # 必须手动编译，但是本机预编译的OpenCV-4.7.0针对CUDA11.6，暂时无法使用find_package()
    # 方法来自动寻找OpenCV。如果要使用find_package()方法，请重新编译OpenCV（针对
    # CUDA10.2）。
    set(OpenCV_DIR "D:/ProgramData/opencv-win-4.7.0/build")
else ()
    message("-- Load TensorRT on Linux")
    set(TensorRT_INCLUDE_DIRS /usr/include/aarch64-linux-gnu)
    set(TensorRT_LIBS /usr/lib/aarch64-linux-gnu)
    message("-- Load OpenCV on Linux")
endif ()
find_package(OpenCV REQUIRED)
include_directories(${TensorRT_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})

add_executable(CudaDetectTopRight main.cu)
add_executable(train main.cu)
#add_executable(alpha alpha.cpp)
add_executable(beta beta.cpp)
if (WIN32)
    message("Find libraries (nvinfer nvparsers nvonnxparser) on Windows")
    #    target_link_libraries(CudaDetectTopRight "opencv_world470d.lib" "opencv_img_hash470d.lib")
else ()
    message("-- Find libraries (nvinfer nvparsers nvonnxparser) on Linux")
endif ()
find_library(NVINFER nvinfer HINTS ${TensorRT_LIBS})  # 查找nvinfer库
find_library(NVPARSERS nvparsers HINTS ${TensorRT_LIBS})  # 查找nvparsers库
find_library(NVONNXPARSER nvonnxparser HINTS ${TensorRT_LIBS})  # 查找 nvonnxparser 库

target_link_libraries(train ${NVINFER} ${NVPARSERS} ${NVONNXPARSER} ${OpenCV_LIBS})
target_link_libraries(CudaDetectTopRight ${NVINFER} ${NVPARSERS} ${NVONNXPARSER} ${OpenCV_LIBS})

set_target_properties(CudaDetectTopRight PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
set_target_properties(train PROPERTIES CUDA_SEPARABLE_COMPILATION ON)

