#include "detectTR.h"
#include <windows.h>
#include <iostream>
#include <fstream>

int readBufferFromFileW(const wchar_t *fileName, unsigned char *buf, size_t size, unsigned long &dataLen) {
    HANDLE pFile;

    int tryTime = 20;
    for (int i = 0; i < tryTime; i++) {
        if (i > 0) {
            Sleep(300);
        }
        pFile = CreateFileW(fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (INVALID_HANDLE_VALUE != pFile) {
            break;
        }
    }

    if (INVALID_HANDLE_VALUE == pFile) {
        CloseHandle(pFile);
        return -1;
    }

    int file_length1 = GetFileSize(pFile, NULL);
    if (file_length1 == INVALID_FILE_SIZE) {
        CloseHandle(pFile);
        return -1;
    }

    int toRead = 0;
    if (file_length1 <= size) {
        toRead = file_length1;
    } else {
        toRead = size;
    }

    if (ReadFile(pFile, buf, toRead, &dataLen, NULL) == 0) {
        CloseHandle(pFile);
        return -1;
    }

    CloseHandle(pFile);

    return 0;
}

int readBufferFromFile(const char *fileName, unsigned char *buf, size_t size, unsigned long &dataLen) {
    // Convert char to wchar_t
    WCHAR szFileName[1024] = {L'\0'};
    MultiByteToWideChar(CP_ACP, 0, fileName, static_cast<int>(strlen(fileName) + 1), szFileName,
                        sizeof(szFileName) / sizeof(szFileName[0]));

    // Call the wide version of the function
    return readBufferFromFileW(szFileName, buf, size, dataLen);
}

int main() {

    constructNet();
    const char *fileName = R"(C:\Users\13636\OneDrive\01WorkingDirectory\Dataset\InfraredVisionDataset\GenerateFromHere\TopRight\register_00000310.jpg)"; // 替换为你的文件路径

    // 打开文件并确定大小
    std::ifstream file(fileName, std::ifstream::binary | std::ifstream::ate);
    if (!file.is_open()) {

        return -1;
    }

    std::streamsize size = file.tellg();

    file.close();

    ImgData imgData;
    imgData.height = 2592;
    imgData.width = 1944;
    imgData.dataType = 0;
    // 分配内存并设置 dataLength
    imgData.data = new unsigned char[size];
    imgData.dataLength = static_cast<unsigned int>(size);

    // 使用 readBufferFromFile 读取数据
    unsigned long readLength = 0;
    int result = readBufferFromFile(fileName, imgData.data, imgData.dataLength, readLength);

    // （可选）确认实际读取的长度
    // 如果你想验证实际读取的数据量是否和预期相符，可以在这里检查 readLength


    if (result != 0) {
        std::cout << "fail" << std::endl;
        delete[] imgData.data; // 记得释放内存
        return -1;
    }
    bool i = inference(imgData);
    delete[] imgData.data;

    std::cout << i << std::endl;

    freeSource();
    return 0;
}