import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import load_img, img_to_array


def preprocess_input(image_path, image_height, image_width):
    """加载和预处理输入图像"""
    img = load_img(image_path, target_size=(image_height, image_width), color_mode="grayscale")
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img /= 255.0
    return img


def postprocess_output(pred):
    """处理模型的预测结果"""
    return np.argmax(pred, axis=1)[0]


def run_inference(model_path, image_path, image_height, image_width):
    """加载模型，处理输入，运行推理，处理输出"""
    model = load_model(model_path)

    img = preprocess_input(image_path, image_height, image_width)

    pred = model.predict(img)

    result = postprocess_output(pred)

    return result


if __name__ == '__main__':
    model_path = 'path_to_your_model'
    image_path = 'path_to_your_image'
    image_height = 224
    image_width = 224
    result = run_inference(model_path, image_path, image_height, image_width)
    print(f'The predicted class is: {result}')
