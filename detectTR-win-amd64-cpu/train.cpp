#include <iostream>
#include <chrono>
#include "base_model.h"
#include "data_preprocessor.h"

int mai1() {
    CoDeNet net;
    torch::Device device = torch::kCPU;
    if (torch::cuda::is_available()) {
        std::cout << "CUDA is available! Training on GPU." << std::endl;
        device = torch::kCUDA;
    }
    torch::load(net, "../conet.pt");
    net->to(device);
    std::string path_train = R"(D:\01WorkingDirectory\train\)";
    std::string path_val = R"(D:\01WorkingDirectory\val\)";
    std::string annotations_train = R"(../train.txt)";
    std::string annotations_val = R"(../val.txt)";
    auto custom_dataset_train = DataLoader(path_train, annotations_train, 1, 224, 224).map(
            torch::data::transforms::Stack<>());;
    auto data_loader_train = torch::data::make_data_loader<torch::data::samplers::RandomSampler>(
            std::move(custom_dataset_train), torch::data::DataLoaderOptions(64));
    auto custom_dataset_val = DataLoader(path_val, annotations_val, 1, 224, 224).map(
            torch::data::transforms::Stack<>());;
    auto data_loader_val = torch::data::make_data_loader<torch::data::samplers::RandomSampler>(
            std::move(custom_dataset_val), torch::data::DataLoaderOptions(32));
    auto loss_function = torch::nn::BCELoss();
    torch::optim::Adam optimizer(net->parameters(), torch::optim::AdamOptions(0.001));

    int num_epochs = 10;
    for (size_t epoch = 1; epoch <= num_epochs; ++epoch) {

        float total_correct_train = 0;
        float total_correct_val = 0;
        float loss_train = 0;
        float loss_val = 0;
        size_t batch_index_train = 0;
        size_t batch_index_val = 0;
        for (auto &batch: *data_loader_train) {
            auto data = batch.data.to(torch::kF32).to(device);
            auto targ = batch.target.to(torch::kF32).to(device);
            auto output = net->forward(data);

            auto loss = loss_function(output, targ);

            optimizer.zero_grad();
            loss.backward();
            optimizer.step();

            loss_train += loss.item<float>();

            auto predictions = output.round(); // 将输出转换为0或1
            total_correct_train += torch::sum(predictions.eq(targ)).item<float>();
            batch_index_train++;

            float batch_loss = loss_train / batch_index_train;
            float batch_accuracy = total_correct_train / (batch_index_train * batch.data.size(0));

            std::cout << "Epoch: " << epoch << " | Batch: " << batch_index_train << " | Average Train Loss: "
                      << batch_loss << " | Average Train Acc: " << batch_accuracy << "\r";
        }
        float epoch_loss = loss_train / batch_index_train;
        float epoch_accuracy = total_correct_train / (batch_index_train * data_loader_train->options().batch_size);
        std::cout << "\nEpoch: " << epoch << " | Average Train Loss: " << epoch_loss << " | Average Train Acc: "
                  << epoch_accuracy << std::endl;
        net->eval();
        torch::NoGradGuard no_grad;
        for (auto &batch: *data_loader_val) {

            auto data = batch.data.to(torch::kF32).to(device);
            auto targ = batch.target.to(torch::kF32).to(device);
            auto output = net->forward(data);

            auto loss = loss_function(output, targ);
            loss_val += loss.item<float>();

            auto predictions = output.round(); // 将输出转换为0或1
            total_correct_val += torch::sum(predictions.eq(targ)).item<float>();
            batch_index_val++;
        }
        float avg_val_loss = loss_val / batch_index_val;
        float avg_val_accuracy = total_correct_val / (batch_index_val * data_loader_val->options().batch_size);

        std::cout << "Epoch: " << epoch << " | Average Validation Loss: " << avg_val_loss
                  << " | Average Validation Acc: " << avg_val_accuracy << std::endl;
    }
    net->to(torch::kCPU);
    torch::save(net, "../conet.pt");
    return 0;
}
