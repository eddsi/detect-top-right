import os
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from ..utils.config_manager import ConfigManager
from .base_model import CoDeNet_Sequential


class ModelTrainer:
    def __init__(self, data_dir, config_manager, model_path=None):
        for key, value in config_manager.config.items():
            setattr(self, key, value)
        self.data_dir = data_dir
        self.model_path = model_path
        self.train_datagen = ImageDataGenerator(rescale=1. / 255, validation_split=0.2)
        os.environ['CUDA_VISIBLE_DEVICES'] = "0"

    def load_data(self):
        self.train_generator = self.train_datagen.flow_from_directory(
            self.data_dir, target_size=(self.image_height, self.image_width),
            color_mode="grayscale", class_mode='binary',
            subset='training', batch_size=self.batch_size)
        self.validation_generator = self.train_datagen.flow_from_directory(
            self.data_dir, target_size=(self.image_height, self.image_width),
            color_mode="grayscale", class_mode='binary',
            subset='validation', batch_size=self.batch_size)

    def init_model(self):
        if self.model_path is None:
            model = CoDeNet_Sequential()
            self.model = model.get_model()
        else:
            self.model = load_model(self.model_path)

    def train_model(self, epochs=10):
        self.model.fit(self.train_generator,
                       steps_per_epoch=self.train_generator.samples // self.train_generator.batch_size,
                       validation_steps=self.validation_generator.samples // self.validation_generator.batch_size,
                       validation_data=self.validation_generator, epochs=epochs)

    def save_model(self):
        if self.model_path is None:
            self.model.save(f"../model_weights/{self.model.name}.h5")
        else:
            self.model.save(self.model_path)

    def run(self):
        self.init_model()
        self.load_data()
        self.train_model(epochs=1)
        self.save_model()
        print("success")


if __name__ == '__main__':
    config_manager = ConfigManager("../src/config/training.json")
    trainer = ModelTrainer(data_dir=r"D:\01WorkingDirectory\train",
                           config_manager=config_manager,
                           model_path="../../model_weights/conet.h5")
    trainer.run()
