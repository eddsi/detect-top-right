#include "ModelPersistence.h"
#include <iostream>

int main() {
    const std::string model_path = "../../model_weights/TPClassifier.pt";

    ModelPersistence model_persistence;

    // Load the model
    auto model = model_persistence.loadModel(model_path, torch::kCPU);

    // Process with the model (e.g., forward pass with dummy data)
    // ...

    // Save the model
    const std::string saved_model_path = "../../model_weights/TPClassifier_saved.pt";
    model_persistence.saveModel(saved_model_path, torch::kCPU);

    std::cout << "Model saved to " << saved_model_path << std::endl;

    return 0;
}
