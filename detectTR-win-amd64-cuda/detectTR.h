#ifndef DETECTTR_WIN_AMD64_CUDA_LIBRARY_H
#define DETECTTR_WIN_AMD64_CUDA_LIBRARY_H
struct ImgData {
    unsigned char *data;
    unsigned int dataLength;
    int height;
    int width;
    int dataType;//图像类型 （0:jpeg, 1:bmp, 2:png, 3:tiff.）
};

bool constructNet();

bool inference(ImgData img);

void freeSource();

#endif //DETECTTR_WIN_AMD64_CUDA_LIBRARY_H
