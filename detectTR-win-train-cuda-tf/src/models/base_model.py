from abc import ABC, abstractmethod
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam


class IModel(ABC):
    @abstractmethod
    def create_model(self):
        pass

    @abstractmethod
    def compile_model(self, model: Model):
        pass

    @abstractmethod
    def get_model(self) -> Model:
        pass


class CoDeNet_Sequential(IModel):
    def __init__(self):
        self.model = self.create_model()
        self.compile_model()

    def create_model(self):
        model = Sequential(name="CoDeNet")
        model.add(Input(shape=(224, 224, 1)))
        model.add(Conv2D(16, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Conv2D(16, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(Conv2D(512, kernel_size=(3, 3), activation='relu', padding='same'))
        model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))

        model.add(Flatten())

        model.add(Dense(64, activation='relu'))
        model.add(Dense(64, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        return model

    def compile_model(self, optimizer=Adam(learning_rate=0.0001), loss='binary_crossentropy', metrics=['accuracy']):
        model = self.model
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def get_model(self) -> Model:
        return self.model


class XXNet(IModel):
    def __init__(self):
        self.model = self.create_model()
        self.compile_model()

    def create_model(self):
        input_layer = Input(shape=(224, 224, 1))
        x = Conv2D(16, kernel_size=(3, 3), activation='relu', padding='same')(input_layer)
        x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

        x = Conv2D(16, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = Conv2D(32, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

        x = Conv2D(32, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

        x = Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

        x = Conv2D(128, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = Conv2D(512, kernel_size=(3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D(pool_size=(2, 2), padding='same')(x)

        x = Flatten()(x)
        x = Dense(64, activation='relu')(x)
        x = Dense(64, activation='relu')(x)
        output_layer = Dense(1, activation='sigmoid')(x)

        model = Model(inputs=input_layer, outputs=output_layer)

        model.summary()
        model.compile(optimizer=Adam(learning_rate=0.0001), loss='binary_crossentropy', metrics=['accuracy'])
        return model

    def compile_model(self, optimizer=Adam(learning_rate=0.0001), loss='binary_crossentropy', metrics=['accuracy']):
        model = self.model
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def get_model(self) -> Model:
        return self.model
