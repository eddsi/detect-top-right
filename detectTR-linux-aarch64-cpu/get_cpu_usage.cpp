#include <windows.h>
#include <stdio.h>
#include <torch/torch.h>
#include <iostream>
#include <chrono>
#include "base_model.h"
#include "data_preprocessor.h"

FILETIME ftime, fsys, fuser;
ULARGE_INTEGER now, sys, user;
ULARGE_INTEGER lastSysCPU, lastUserCPU, lastCPU;
double percent;
int numProcessors;
SYSTEM_INFO sysInfo;

// 获取系统信息
GetSystemInfo(&sysInfo);
numProcessors = sysInfo.dwNumberOfProcessors;

// 获取初始系统时间
GetSystemTimeAsFileTime(&ftime);
memcpy(&lastCPU, &ftime, sizeof(FILETIME));

// 获取初始进程时间
GetProcessTimes(GetCurrentProcess(), &ftime, &ftime, &fsys, &fuser);
memcpy(&lastSysCPU, &fsys, sizeof(FILETIME));
memcpy(&lastUserCPU, &fuser, sizeof(FILETIME));

// 等待一段时间以便计算
Sleep(100); // 等待100毫秒

// 再次获取系统和进程时间
GetSystemTimeAsFileTime(&ftime);
memcpy(&now, &ftime, sizeof(FILETIME));

GetProcessTimes(GetCurrentProcess(), &ftime, &ftime, &fsys, &fuser);
memcpy(&sys, &fsys, sizeof(FILETIME));
memcpy(&user, &fuser, sizeof(FILETIME));

// 计算CPU使用率
percent = (double)(sys.QuadPart - lastSysCPU.QuadPart) +
          (double)(user.QuadPart - lastUserCPU.QuadPart);
percent /= (double)(now.QuadPart - lastCPU.QuadPart);
percent /= numProcessors;

// 输出结果
printf("CPU usage: %f %%\n", percent * 100);
