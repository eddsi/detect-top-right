#ifndef DETECTTR_WIN_TRAIN_CUDA_TORCH_DATA_PREPROCESSOR_H
#define DETECTTR_WIN_TRAIN_CUDA_TORCH_DATA_PREPROCESSOR_H

#include <torch/library.h>
#include <torch/data/datasets/base.h>
#include <opencv2/opencv.hpp>

class DataLoader : public torch::data::datasets::Dataset<DataLoader> {
public:
    DataLoader(std::string img_dir, const std::string &annotations_file, int channel, int height, int width);

    torch::data::Example<> get(size_t index) override;

    torch::optional<size_t> size() const override;

    long get_num_classes();

    torch::Tensor img_to_tensor(cv::Mat img);

private:
    std::vector<std::string> images_list;
    std::vector<int64_t> labels_list;
    long num_classes;
    int out_channels;
    int out_height;
    int out_width;
    int flag_open;
};
#endif //DETECTTR_WIN_TRAIN_CUDA_TORCH_DATA_PREPROCESSOR_H
