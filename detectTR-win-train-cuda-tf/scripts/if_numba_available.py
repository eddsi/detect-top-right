from numba import cuda


def cpu_print():
    x = 3
    print(f"Hello, this is cpu!{x}")


@cuda.jit
def gpu_print():
    x, y = cuda.grid(2)
    print("NVIDIA GTX 1660 Super gets started!")


if __name__ == '__main__':
    gpu_print[2, 3]()
    cuda.synchronize()
    cpu_print()
